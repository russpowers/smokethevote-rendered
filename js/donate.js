define([], function() {
	'use strict';

	

	return function() {
		var $paypalDonateButton = $('#donatePaypalBtn');
		var $btcDonateButton = $('#donateBtnBtn');
		var $paypalDonation = $('#paypalDonation');
		var $btcDonation = $('#btcDonation');
		var $extraData = $('.extraData');
		var $fecOk = $('#fecOk');
		var $cbButton = $('#cbButton');

		var $fullname = $('#fullname');
		var $employer = $('#employer');
		var $occupation = $('#occupation');
		var $fullnameInput = $('#fullname input');
		var $employerInput = $('#employer input');
		var $occupationInput = $('#occupation input');
		var $ppDisclosureName = $('#os0');
		var $ppDisclosureEmployerOccupation = $('#os1');


		var donationType;
		var dataComplete = false;


		$fecOk.on('click', function() {

			var hasErrors = false;
			if (!$fullnameInput.val()) {
				$fullname.addClass('has-error');
				hasErrors = true;
			} else {
				$fullname.removeClass('has-error');
			}

			if (!$employerInput.val()) {
				$employer.addClass('has-error');
				hasErrors = true;
			} else {
				$employer.removeClass('has-error');
			}

			if (!$occupationInput.val()) {
				$occupation.addClass('has-error');
				hasErrors = true;
			} else {
				$occupation.removeClass('has-error');
			}

			if (hasErrors) {
				return;
			}

			$ppDisclosureName.val($fullnameInput.val());
			$ppDisclosureEmployerOccupation.val(
				$employerInput.val() + ' / ' + $occupationInput.val());

			$cbButton.html('<a class="coinbase-button" data-custom="'+
				$fullnameInput.val().replace('"', '') + ' / ' + $employerInput.val().replace('"', '') + " / " + $occupationInput.val().replace('"', '') + 
				'" data-code="8bf1a00748911620843aea280674d5aa" data-button-style="donation_large" href="https://coinbase.com/checkouts/8bf1a00748911620843aea280674d5aa">Donate Bitcoins</a><script src="https://coinbase.com/assets/button.js" type="text/javascript"></script>')

			dataComplete = true;
			$extraData.hide();
			
			if (donationType === 'paypal') {
				$paypalDonation.show();
				$btcDonation.hide();
			} else if (donationType === 'btc') {
				$paypalDonation.hide();
				$btcDonation.show();
			}
		});


		$paypalDonateButton.on('click', function() {
			donationType = 'paypal';
			$paypalDonateButton.addClass('active');
			$btcDonateButton.removeClass('active');
			if (dataComplete) {
				$paypalDonation.show();	
				$btcDonation.hide();
			} else {
				$extraData.show();
			}
		});

		$btcDonateButton.on('click', function() {
			donationType = 'btc';
			$paypalDonateButton.removeClass('active');
			$btcDonateButton.addClass('active');
			if (dataComplete) {
				$btcDonation.show();	
				$paypalDonation.hide();
			} else {
				$extraData.show();
			}
		});
	};
});