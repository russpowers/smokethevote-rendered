define([], function() {
	'use strict';

	

	return function() {
		var $showDemocrats = $('.show-democrats');
		var $showRepublicans = $('.show-republicans');
		var $showAll = $('.show-all');

		var $democratRaces = $('#main-container .race-container.party-D');
		var $republicanRaces = $('#main-container .race-container.party-R');
		var $otherRaces = $('#main-container .race-container.party-');

		var $showIncumbents = $('.show-incumbents');
		var $showEndorsed = $('.show-endorsed');
		var $showEveryone = $('.show-everyone');

		var $incumbentCandidatesBig = $('#main-container .candidate.incumbent');
		var $incumbentCandidatesSmall = $('#main-container .candidate-small.incumbent');
		var $endorsedCandidates = $('#main-container .candidate.endorsed');
		var $otherCandidatesBig = $('#main-container .candidate:not(.incumbent,.endorsed)');
		var $otherCandidatesSmall = $('#main-container .candidate-small:not(.incumbent,.endorsed)');

		$showDemocrats.on('click', function() {
			$democratRaces.show();
			$republicanRaces.hide();
			$otherRaces.hide();
		});

		$showRepublicans.on('click', function() {
			$democratRaces.hide();
			$republicanRaces.show();
			$otherRaces.hide();
		});

		$showAll.on('click', function() {
			$democratRaces.show();
			$republicanRaces.show();
			$otherRaces.show();
		});

		$showIncumbents.on('click', function() {
			$endorsedCandidates.hide();
			$otherCandidatesBig.hide();
			$otherCandidatesSmall.hide();
			$incumbentCandidatesBig.show();
			$incumbentCandidatesSmall.show();
		});

		$showEndorsed.on('click', function() {
			$otherCandidatesBig.hide();
			$otherCandidatesSmall.hide();
			$incumbentCandidatesBig.hide();
			$incumbentCandidatesSmall.hide();
			$endorsedCandidates.show();
		});

		$showEveryone.on('click', function() {
			$endorsedCandidates.show();
			$otherCandidatesBig.show();
			$otherCandidatesSmall.show();
			$incumbentCandidatesBig.show();
			$incumbentCandidatesSmall.show();
		});

	};
});