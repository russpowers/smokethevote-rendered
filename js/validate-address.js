define(['liveaddress'], function(liveaddress) {
	'use strict';

	return function() {

		var $input = $('#big-input');
		var $go = $('#go-button');
		var $loader = $('#loading');


		// Loader stuff
		var showLoader = function() {
			$loader.addClass('show');
		}

		var hideLoader = function() {
			$loader.removeClass('show');
		}


		// Tooltip stuff
		var tooltipVisible = false;
		var hasTooltip = false;

		var showTooltip = function(message) {
			if (hasTooltip) {
				$input.tooltip('destroy');
			}

			$input.tooltip({
				title: message,
				trigger: 'manual'
			});

			$input.tooltip('show');
			hasTooltip = true;
			tooltipVisible = true;
		}

		var hideTooltip = function() {
			if (tooltipVisible) {
				$input.tooltip('hide');
				tooltipVisible = false;
			}
		}

		var parseAddress = function() {
			var address = $.trim($input.val());

			if (address.length === 0) {
				showTooltip("Enter a 9-digit ZIP or a full street address");
				return false;
			}

			var matches;

			matches = address.match(/(\d\d\d\d\d)[-\s]?(\d\d\d\d)/);

			if (matches) {
				return { zip: matches[1] + matches[2] };
			}

			matches = address.match(/^\d\d\d\d\d$/);

			if (matches) {
				showTooltip("Enter a full 9-digit ZIP, like 12345-6789");
				return false;
			}

			if (!address.match(/\d\d\d\d\d/)) {
				showTooltip("Address should be in the format 123 Main St. City State ZIP");
				return false;
			}

			return { address: address };
		}

		var checkCodeAndRedirect = function(zip) {
			$.ajax('/check/' + zip)
				.done(function() {
					window.location.href = '/info/' + zip;
				})
				.fail(function(err) {
					hideLoader();
					if (err.status === 400) {
						showTooltip('Could not find the zip code ' + zip.substr(0, 5) + '-' + zip.substr(5, 4) + ' in our database.');
					} else if (err.status === 500) {
						showTooltip('There was a problem processing your request.  Please try again later.')
					} else {
						showTooltip('There was a problem contacting the server. Is you internet connection working?');
					}
				});
		}
		
		var go = function() {
			var result = parseAddress();
			if (result) {
				showLoader();
				if (result.address) {
				
					liveaddress.verify(result.address, function(data) {
						if (!data || data.length === 0) {
							hideLoader();
							showTooltip('Address not found, please check it and try again');
						} else {
							var zip = data[0].components.zipcode+data[0].components.plus4_code;

							if (data.length > 1) {
								for (var i = 1; i < data.length; ++i) {
									if (data[i].components.zipcode + data[i].components.plus4_code !== zip) {
										hideLoader();
										return showTooltip('Multiple addresses found, please check it and try again');
									}
								}
							}

							checkCodeAndRedirect(zip);
						}
					}, function() { 
						hideLoader();
						return showTooltip('There was a problem contacting the server.  Is you internet connection working?');
					});
				
				} else if (result.zip) {
					checkCodeAndRedirect(result.zip);
				}
			}
		};

		$go.on('click', function() {
			go();
		});

		$input.on('keyup', function(e) {
			hideTooltip();
			if (e.keyCode === 13) {
				go();
			}
		});

	};
});