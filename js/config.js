'use strict';


requirejs.config({
    paths: {
		//'twitter-bootstrap': '../components/bootstrap/dist/js/bootstrap',
		'bootstrap-tooltip': '../components/bootstrap/js/tooltip',
		'bootstrap-button': '../components/bootstrap/js/button',
		'bootstrap-collapse': '../components/bootstrap/js/collapse',
		'liveaddress': '../lib/liveaddress/liveaddress'
	},

	deps: ['./app']
});
