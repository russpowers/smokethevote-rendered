define(['require',
	'liveaddress',
	'./validate-address',
	'./email-subscribe',
	'./toggles',
	'./donate',
	'bootstrap-tooltip',
	'bootstrap-button',
	'bootstrap-collapse'
], function (require, liveaddress, validateAddress, emailSubscribe, toggles, donate) {
	'use strict';

	$(document).ready(function() {
		liveaddress.init('5709446201813566380');
		validateAddress();
		emailSubscribe();
		toggles();
		donate();
	});

  /*  var app = {
        initialize: function () {
            // Your code here
        }
    };

    app.initialize();*/

});


