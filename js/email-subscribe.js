define([], function() {
	'use strict';

	

	return function() {
		var $emailButton = $('#email-button');
		var $emailRow = $('#email-row');
		var $emailInput = $('#email-input');

		$('a').click(function(){
			if ($.attr(this, 'anchor')) {
		    $('html, body').animate({
		        scrollTop: $( $.attr(this, 'anchor') ).offset().top - 55
		    }, 500);
		    //return false;
		  }
		});

		$('a').each(function(index, el) {
			var href = $(el).attr('href');
			if (href.indexOf('/tellus') === 0) {
				$(el).attr('href', href+'?source=' + encodeURIComponent(window.location.href));
			}
		});

		$emailInput.tooltip({
			title: "There was an error, please try again later.  Sorry!",
			trigger: 'manual'
		});

		$emailButton.on('click', function() {
			$emailInput.tooltip('hide');
			$.ajax({
				type: 'POST',
				url: '/subscribe/' + window.zipCode,
				data: { email: $emailInput.val() }
			}).done(function() {
				$emailRow.addClass('done');	
			})
			.fail(function(err) {
				$emailInput.tooltip('show');
			});
		});
	};
});